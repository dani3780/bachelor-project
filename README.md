# Learning Management System

## Getting started

### Requirements

* On a new machine install or if not installed yet: 
```
sudo apt install git
```

```
sudo apt install npm
```

MongoDB according to the official documentation
```
https://docs.mongodb.com/manual/tutorial/install-mongodb-on-ubuntu/
```

### Installation

* Clone the gitlab repository
```
git clone https://gitlab.com/dani3780/bachelor-project.git
```
* Access mongoDB within the terminal
```
mongo
```
* Create the database 'lms'
```
use lms;
```

* Install dependencies within the /vue folder
```
npm install
```

* fix dependencies within the /vue folder
```
npm audit fix
```

* Install dependencies at the backend within the /vue/backend folder
```
npm install
```

* fix dependencies at the backend within the /vue/backend folder
```
npm audit fix
```

* adapt the database connection string, if authentication is required on your machine within the /vue/backend/database.js file



### Starting services

* Vue.js within the /vue folder
```
npm run serve
```

* Mongo.db database within the /vue folder
```
mongo
```

* Node.js server within the /vue/backend folder
```
npx nodemon app.js
```

## Testing

* Install mocha for starting Chakram testing
```
sudo npm install -g mocha --save-dev
```

* Ensure database service and Node.js server is running.

* Start test suites within the /vue/backend folder
```
mocha test
```

