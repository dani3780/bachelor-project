var chakram = require('chakram'),
    expect = chakram.expect;

describe("authAPI", function () {

    //initially insert _test_ user to ensure the account exists
    it("signUp functionality", function () {
        chakram.post("http://localhost:4000/api/auth/signup", { "username": "_test_", "email": "_test_@test.de", "password": "_test_" });
        // chakram.wait();
        // var response = chakram.post("http://localhost:4000/api/auth/signup", { "username": "_test_", "email": "_test_@test.de", "password": "_test_" });
        // return expect(response).to.have.status(400);
    });

    //valid login, expect statuscode 200
    it("login functionality", function () {
        var response = chakram.post("http://localhost:4000/api/auth/signin", { "username": "_test_", "password": "_test_" });
        return expect(response).to.have.status(200);
    });

    //provide invalid tokens, expect statuscode 401
    it("invalid token", function () {
        const course = {};
        var response = chakram.post("http://localhost:4000/api/course/create-course", course, { headers: { 'x-access-token': 'data.accessToken', 'x-access-signature': 'data.signature' } });
        return expect(response).to.have.status(401);
    });

    //provide invalid login credentials, expect statuscode 401
    it("wrong login credentials", function () {
        const course = {};
        var response = chakram.post("http://localhost:4000/api/auth/signin", { "username": "_test_", "password": "asdf" });
        return expect(response).to.have.status(401);
    });

    //provide no authentication data, expect statuscode 403
    it("unauthorized access", function () {
        var response = chakram.get("http://localhost:4000/api/course/get-courses");
        return expect(response).to.have.status(403);
    });
});