var chakram = require('chakram'),
    expect = chakram.expect;
describe("courseAPI", function () {
    var course_name = "_test_course_10";
    //default course object
    var course = {
        title: course_name,
        author: null,
        year: 2021,
        keywords: [],
        prerequisites: [],
        difficulty: "Intermediate",
        duration: null,
        language: "English",
        description: null,
        chapters: [
            {
                name: "",
                documents: [],
                courses: [],
            },
        ],
    };
    it("signUp functionality", function () {
        chakram.post("http://localhost:4000/api/auth/signup", { "username": "_test_", "email": "_test_@test.de", "password": "_test_" });
        var response = chakram.post("http://localhost:4000/api/auth/signup", { "username": "_test_", "email": "_test_@test.de", "password": "_test_" });
        //at second registration process with same credentials, error 400 is sent.
        return expect(response).to.have.status(400);
    });
    
    //login to get valid authentication tokens
    it("login functionality", function () {
        return chakram.post("http://localhost:4000/api/auth/signin", { "username": "_test_", "password": "_test_" }).
            then(function (loginResponse) {
                var data = loginResponse.body;
                accessToken = data.accessToken;
                signature = data.signature;
                expect(loginResponse).to.have.status(200);
            });
    });
    //testing the creation of courses
    it("create-course", function () {
        //define the concrete REST call with the HTTP method, body and header
        //assign authentication data
        var response = chakram.post("http://localhost:4000/api/course/create-course", course, { headers: { 'x-access-token': accessToken, 'x-access-signature': signature } });
        expect(response).to.have.status(200); //validate status
        expect(response).to.not.have.status(500);
        expect(response).to.not.have.status(403);
        expect(response).to.comprise.of.json({ //validate content
            title: course_name,
            year: "2021",
            difficulty: "Intermediate",
        });
        return chakram.wait();
    });
    //check for unauthorized access to courses
    it("create-course unauthorized", function () {
        var response = chakram.post("http://localhost:4000/api/course/create-course", course);
        return expect(response).to.have.status(403);
    });

    var accessToken = "";
    var signature = "";
    var foundCourse = "";
    //search for courses in advance to get an objectId to get and update a course
    it("search-course", function () {
        var body = { "query": course_name };
        return chakram.post("http://localhost:4000/api/course/search", body, { headers: { 'x-access-token': accessToken, 'x-access-signature': signature } }).
            then(function (searchResponse) {
                expect(searchResponse).to.have.status(200);
                foundCourse = searchResponse.body[0];
                expect(foundCourse).to.include({ "title": course_name, "year": "2021" });
            });


    });
    //update a previously created course
    it("update-course", function () {
        var updated_name = "_test_course_updated4";
        var updated_course = {
            _id: foundCourse._id,
            title: updated_name,
            author: "_test_",
            year: "2021",
            keywords: [],
            prerequisites: [],
            difficulty: "Intermediate",
            duration: null,
            language: "English",
            description: null,
            chapters: [
                {
                    name: "",
                    documents: [],
                    courses: [],
                },
            ],
        };
        //further validate the response with the keyword then
        return chakram.post("http://localhost:4000/api/course/update-course/" + foundCourse._id, updated_course, { headers: { 'x-access-token': accessToken, 'x-access-signature': signature } })
            .then(function (createResponse) {
                expect(createResponse).to.have.status(200);

                return chakram.get("http://localhost:4000/api/course/get-course?query=" + foundCourse._id, { headers: { 'x-access-token': accessToken, 'x-access-signature': signature } }).
                    then(function (getResponse) {
                        expect(getResponse).to.have.status(200);
                        expect(getResponse.body).to.include({ "title": updated_name, "year": "2021" });
                        expect(getResponse.body).to.not.include({ "title": course_name });
                    });
            });
    });

    it("delete-course", function () {
        var body = { "query": foundCourse._id };
        return chakram.delete("http://localhost:4000/api/course/delete-course?courseId=" + foundCourse._id, body, { headers: { 'x-access-token': accessToken, 'x-access-signature': signature } }).
            then(function (deleteResponse) {
                expect(deleteResponse).to.have.status(200);
                // foundCourse = deleteResponse.body[0];
                // expect(foundCourse).to.include({ "title": course_name, "year": "2021" });
            });
    });
});
