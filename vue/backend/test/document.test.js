var chakram = require('chakram'),
    expect = chakram.expect;
//Testing the documentAPI including authentication with valid user credentials
describe("documentAPI", function () {
    var accessToken = ""; //authentication data will be stored here
    var signature = "";
    const document_name = "_test_document_3";
    const document = { //defining default document
        title: document_name,
        author: "",
        year: "",
        university: "",
        keywords: ["abc"],
        prerequisites: [],
        difficulty: "Intermediate",
        duration: "1 - 2 hours",
        description: "",
    };
    //signup with a new account
    it("signUp functionality", function () {
        chakram.post("http://localhost:4000/api/auth/signup", { "username": "_test_", "email": "_test_@test.de", "password": "_test_" });
        var response = chakram.post("http://localhost:4000/api/auth/signup", { "username": "_test_", "email": "_test_@test.de", "password": "_test_" });
        //at second registration process with same credentials, error 400 is sent.
        return expect(response).to.have.status(400);
    });
    //login to get valid authentication tokens
    it("login functionality", function () {
        return chakram.post("http://localhost:4000/api/auth/signin", { "username": "_test_", "password": "_test_" }).
            then(function (loginResponse) {
                var data = loginResponse.body;
                accessToken = data.accessToken;
                signature = data.signature;
                expect(loginResponse).to.have.status(200);
            });
    });
    //upload document and check for metadata
    it("upload-document", function () {
        const docData = JSON.stringify(document); //format data for the server
        const res = {
            "metadata": docData,
        }
        var response = chakram.post("http://localhost:4000/api/document/upload-file", res, { headers: { 'x-access-token': accessToken, 'x-access-signature': signature } });
        expect(response).to.have.status(406); //missing parameter (missing file)
        return chakram.wait();
    });
    //search for documents to retrieve the according objectIds to test the get-document method in advance
    it("search-document", function () {
        var body = { "query": document_name };
        return chakram.post("http://localhost:4000/api/document/search", body, { headers: { 'x-access-token': accessToken, 'x-access-signature': signature } }).
            then(function (searchResponse) {
                expect(searchResponse).to.have.status(200);
                foundDocument = searchResponse.body[0];
                expect(foundDocument).to.include({ "title": document_name, "duration": "1 - 2 hours", });
            });
    });
    //retrieve one specific document and check for specified metadata
    it("get-document", function () {
        return chakram.get("http://localhost:4000/api/document/get?query=" + foundDocument._id, { headers: { 'x-access-token': accessToken, 'x-access-signature': signature } }).
            then(function (getResponse) {
                expect(getResponse).to.have.status(200);
                expect(getResponse.body).to.include({ "title": document_name, "difficulty": "Intermediate", });
            });
    });
    it("delete-document", function () {
        var body = { "query": foundDocument._id };
        return chakram.delete("http://localhost:4000/api/document/delete-document?documentId=" + foundDocument._id, body, { headers: { 'x-access-token': accessToken, 'x-access-signature': signature } }).
            then(function (deleteResponse) {
                expect(deleteResponse).to.have.status(200);
            });
    });
});