/*
 * The course route is the entry point for all course related use cases. These use cases include the retrievement,
creating, edition and deletion of courses.
 */

const express = require('express');
const courseRoute = express.Router();
const { auth_jwt_token } = require("../authentication");
const { auth_jwt } = require("../authentication/auth");

let courseModel = require('../models/Course');
let userModel = require('../models/authentication/User');
let roleModel = require('../models/authentication/Role');

/*
DEPRECEATED, was used earlier before user management was implement to simulate an overview page.
*/
courseRoute.route('/get-courses').get([auth_jwt_token.verifyToken], (req, res) => {
  courseModel.find((error, data) => {
    if (error) {
      return next(error)
    } else {
      res.json(data)
    }
  })
});

/*
This use case is used to retrieve a specifc course, requiring the courseID as parameter of the HTTP GET call.
The first parameter is required to check the validity of the user token, which is sent with every call.
*/
courseRoute.route('/get-course').get([auth_jwt_token.verifyToken], (req, res, next) => {
  courseModel.findOne({ "_id": req.query.query }, (error, data) => {
    if (error) {
      return next(error)
    } else {
      if (!data) {
        return res.status(500).send(new Error('not found!'));
      }
      res.json(data);
    }
  }).populate('chapters.documents').populate('chapters.courses');
});

let documentModel = require('../models/Document');

courseRoute.route('/create-course').post([auth_jwt_token.verifyToken], (req, res, next) => {
  //the user object is created to afterwards automatically set the author of a course, depending on the token
  userModel.findOne({ _id: req.userId }, (err, user) => {

    if (err) {
      console.log(err);
      res.status(500).send({ message: err });
      return;
    }
    if (!user) {
      return res.status(500).send(new Error('not found!'));
    }
    var tags = req.body['keywords'];
    var course = req.body;
    //initialize the discussion array
    course.discussions = [];
    course.author = user.username;
    // console.log(course);
    courseModel.create(course, (error, data) => {
      if (error) {
        return next(error)
      } else {
        // console.log(data)
        res.json(data)
      }
    })

  });
});

/*
This use case describes the suggestion of individualized assets. Therefore the documents and courses are sorted
by the amount of hits with the passed keywords. The nested calls are required due to the asynchronous processing of
each call.
*/
courseRoute.route('/propose').post([auth_jwt_token.verifyToken], (req, res, next) => {
  var tags = req.body;
  var courseId = req.query.courseId;
  var response = {
    courses: [],
    documents: [],
  }
  if (tags.length != 0) {
    //sort by amount of keyword hits
    var query = documentModel.aggregate([
      { $match: { "keywords": { "$in": tags } } },
      { $unwind: "$keywords" },
      { $match: { "keywords": { "$in": tags } } },
      { $group: { _id: "$_id", numRelTags: { $sum: 1 } } },
      { $sort: { numRelTags: -1 } }
      , { $limit: 10 }
    ])
    query.exec(function (err, documentIds) {
      if (err) {
        console.log(err)
        return err;
      } else {
        //find the corresponding documents with the matching documentIds
        documentModel.find({ "_id": { $in: documentIds } }, function (errFind, documents) {
          if (errFind) {
            console.log(errFind);
            return errFind;
          } else {
            //sort by amount of keyword hits
            var queryCourse = courseModel.aggregate([
              { $match: { "keywords": { "$in": tags } } },
              { $unwind: "$keywords" },
              { $match: { "keywords": { "$in": tags } } },
              { $group: { _id: "$_id", numRelTags: { $sum: 1 } } },
              { $sort: { numRelTags: -1 } }
              , { $limit: 10 }
            ]);
            queryCourse.exec(function (errCourse, courseIds) {
              if (errCourse) {
                console.log(errCourse)
                return errCourse;
              } else {
                //find the corresponding courses with the matching courseIds
                courseModel.find({ "_id": courseIds }, function (errCourseFind, courses) {
                  if (errCourseFind) {
                    console.log(errCourseFind);
                    return errCourseFind;
                  } else {
                    response['documents'] = documents;
                    response['courses'] = courses;
                    res.json(response);
                  }
                });

              }
            });
          }
        });


      }
    });
  } else {
    res.json(response);
  }
});

/*
Use case for updating a course.
*/
courseRoute.route('/update-course/:id').post([auth_jwt_token.verifyToken], (req, res, next) => {
  var course = req.body;
  userModel.findOne({ _id: req.userId }, (err, user) => {
    if (err) {
      console.log(err);
      res.status(500).send({ message: err });
      return;
    }
    if (!user) {
      return res.status(500).send(new Error('not found!'));
    }
    roleModel.find(
      {
        _id: { $in: user.roles }
      },
      (err, roles) => {
        if (err) {
          res.status(500).send({ message: err });
          return;
        }
        courseModel.findOne({ _id: req.params.id }, (err, getCourse) => {
          if (err) {
            console.log(err);
            res.status(500).send({ message: err });
            return;
          }
          if (!getCourse) {
            return res.status(500).send(new Error('not found!'));
          }
          //The user either has to be an admin or the author of the course
          if (roles[0].name !== "admin" && getCourse.author !== user.username) {
            console.log(getCourse.author + " vs " + user.username);
            return res.status(401).send({ message: "Unauthorized!" });
          }
          course.author = getCourse.author;
          courseModel.findByIdAndUpdate(req.params.id, {
            $set: course
          }, (error, data) => {
            if (error) {
              return next(error);
            } else {
              res.json(data)
            }
          });
        });
      }
    );
  });
})

/*
A course can also be deleted by the creator or administrators.
*/
courseRoute.route('/delete-course').delete([auth_jwt_token.verifyToken], (req, res, next) => {
  courseModel.findOne({ "_id": req.query.courseId }, (error, course) => {
    if (error) {
      return next(error)
    } else {
      if (!course) {
        return res.status(500).send(new Error('not found!'));
      }
      userModel.findOne({ _id: req.userId }, (err, user) => {
        if (err) {
          console.log(err);
          res.status(500).send({ message: err });
          return;
        }
        if (!user) {
          return res.status(500).send(new Error('not found!'));
        }
        roleModel.find(
          {
            _id: { $in: user.roles }
          },
          (err, roles) => {
            if (err) {
              res.status(500).send({ message: err });
              return;
            }
            //The user either has to be an admin or the author of the course
            if (roles[0].name !== "admin" && course.author !== user.username) {
              return res.status(401).send({ message: "Unauthorized!" });
            }
            courseModel.findByIdAndRemove(req.query.courseId, (error, data) => {
              if (error) {
                return next(error);
              } else {
                res.status(200).json({
                  msg: data
                })
              }
            })
          }
        );
      });
    }
  });
})

/*
This use case is required to search for specific courses and the search term is passed with the body of the request.
POST is used to activate the form of the user interface with the button directly.
*/
courseRoute.route('/search').post([auth_jwt_token.verifyToken], (req, res, next) => {
  userModel.findOne({ _id: req.userId }, (err, user) => {
    if (err) {
      console.log(err);
      res.status(500).send({ message: err });
      return;
    }
    if (!user) {
      return res.status(500).send(new Error('not found!'));
    }
    //regex is used for the inserted search term to expand possible search results.
    courseModel.find({
      $and: [
        {
          $or: [{ "title": { $regex: new RegExp(req.body.query, "i") } }, { "author": { $regex: new RegExp(req.body.query, "i") } },
          { keywords: { $elemMatch: { $regex: new RegExp(req.body.query, "i") } } }]
        }, { "_id": { "$nin": user.courses } }]
    }, function (err, result) {
      if (err) {
        console.log(err)
        return next(err)
      }
      if (result) {
        res.json(result)
      }

    }).limit(25);
  });
});

/*
This use case is required to enroll a user to a course. Therefore the courseId is sent with the call and due to the
validated token the matching user is assigned the regarding courseId.
*/
courseRoute.route('/enroll').post([auth_jwt_token.verifyToken], (req, res, next) => {
  userModel.findOne({ _id: req.userId }, (err, user) => {

    if (err) {
      console.log(err);
      res.status(500).send({ message: err });
      return;
    }
    if (!user) {
      return res.status(500).send(new Error('not found!'));
    }
    if (!user.courses.includes(req.body._id)) {
      user.courses = user.courses.concat(req.body);
      userModel.findByIdAndUpdate(user._id, {
        $set: user
      }, (error, data) => {
        if (error) {
          return next(error);
        } else {
          res.json(data)
        }
      });
    }
  });
});

/*
Within this call the overview of the user of the sent token is returned.
*/
courseRoute.route('/overview').get([auth_jwt_token.verifyToken], (req, res) => {
  userModel.findOne({ _id: req.userId }, (err, user) => {

    if (err) {
      console.log(err);
      res.status(500).send({ message: err });
      return;
    }
    if (!user) {
      return res.status(500).send(new Error('not found!'));
    }
    res.json(user.courses);
  }).populate({ path: 'courses', options: { sort: 'title' } });
});

module.exports = courseRoute;