/*
 * An additional route was created here to fulfill the discussion board related use cases.
These include the retrievement, creation and closing of discussion and furthermore the retrievement,
 creation and deletion of comments, assigned to a discussion.
 Within each call a token is requird and validated. Furthermore are deletion of comments and closing of discussion
 restricted to the author of the element or administrators.
 */

const express = require('express');
const discussionRoute = express.Router();
const { auth_jwt_token } = require("../authentication");


let documentModel = require('../models/Document');
let courseModel = require('../models/Course');
let userModel = require('../models/authentication/User');
let discussionModel = require('../models/Discussion');
let commentModel = require('../models/Comment');
let roleModel = require('../models/authentication/Role');



discussionRoute.route('/get').get([auth_jwt_token.verifyToken], (req, res) => {
    var identifier = "";
    var model;
    if (req.query.documentId !== undefined) {
        identifier = req.query.documentId;
        model = documentModel;
    } else {
        identifier = req.query.courseId;
        model = courseModel;
    }
    model.findOne({ _id: identifier }, (err, document) => {
        if (err) {
            res.status(500).send({ message: err });
            return;
        } else {
            if (!document) {
                return res.status(500).send(new Error('not found!'));
            }
            var opened = document.discussions.filter((discussion) => {
                return discussion.open;
            });
            var closed = document.discussions.filter((discussion) => {
                return !discussion.open;
            });
            var response = {
                "opened": opened,
                "closed": closed,
            }
            res.json(response);
        }
    }).populate({ path: 'discussions', options: { sort: { 'createdAt': -1 }, limit: 20 } });

});

discussionRoute.route('/create').post([auth_jwt_token.verifyToken], (req, res, next) => {
    var identifier = "";
    var model;
    if (req.query.documentId !== undefined) {
        identifier = req.query.documentId;
        model = documentModel;
    } else {
        identifier = req.query.courseId;
        model = courseModel;
    }
    userModel.findOne({ _id: req.userId }, (err, user) => {
        if (err) {
            console.log(err);
            res.status(500).send({ message: err });
            return;
        }
        if (!user) {
            return res.status(500).send(new Error('not found!'));
        }
        var discussion = req.body;
        discussion.author = user.username;
        discussionModel.create(discussion, (error, discussionRes) => {
            if (error) {
                return next(error)
            } else {
                model.findOne({ _id: identifier }, (error, document) => {
                    if (err) {
                        res.status(500).send({ message: err });
                        return;
                    } else {
                        if (!document) {
                            return res.status(500).send(new Error('not found!'));
                        }
                        document.discussions.push(discussionRes);
                        model.findByIdAndUpdate(document._id, {
                            $set: document
                        }, (error) => {
                            if (error) {
                                return next(error);
                            } else {
                                res.json(discussion);
                            }
                        });
                    }
                }).populate('discussions');
            }
        })
    });
});

discussionRoute.route('/close').post([auth_jwt_token.verifyToken], (req, res, next) => {
    
    userModel.findOne({ _id: req.userId }, (err, user) => {
        if (err) {
            console.log(err);
            res.status(500).send({ message: err });
            return;
        }
        if (!user) {
            return res.status(500).send(new Error('not found!'));
        }
        roleModel.find(
            {
                _id: { $in: user.roles }
            },
            (err, roles) => {
                if (err) {
                    res.status(500).send({ message: err });
                    return;
                }
                discussionModel.findOne({ _id: req.query.discussionId }, (err, discussion) => {
                    if (err) {
                        res.status(500).send({ message: err });
                        return;
                    } else {
                        if (!discussion) {
                            return res.status(500).send(new Error('not found!'));
                        }
                        if (roles[0].name !== "admin" && discussion.author !== user.username) {
                            return res.status(401).send({ message: "Unauthorized!" });
                        }
                        discussion.open = false;
                        discussionModel.findByIdAndUpdate(discussion._id, {
                            $set: discussion
                        }, (error) => {
                            if (error) {
                                return next(error);
                            } else {
                                res.json(discussion);
                            }
                        });
                    }
                })

            }
        );
    });
});

discussionRoute.route('/create-comment').post([auth_jwt_token.verifyToken], (req, res, next) => {
    userModel.findOne({ _id: req.userId }, (err, user) => {
        if (err) {
            console.log(err);
            res.status(500).send({ message: err });
            return;
        }
        if (!user) {
            return res.status(500).send(new Error('not found!'));
        }
        var comment = req.body;
        comment.author = user.username;
        commentModel.create(comment, (error, commentRes) => {
            if (error) {
                return next(error)
            } else {
                discussionModel.findOne({ _id: req.query.discussionId }, (error, discussion) => {
                    if (err) {
                        res.status(500).send({ message: err });
                        return;
                    } else {
                        if (!discussion) {
                            return res.status(500).send(new Error('not found!'));
                        }
                        discussion.comments.push(commentRes);
                        discussionModel.findByIdAndUpdate(discussion._id, {
                            $set: discussion
                        }, (error) => {
                            if (error) {
                                return next(error);
                            } else {
                                res.json(comment);
                            }
                        });
                    }
                }).populate('comments');
            }
        })
    });
});

discussionRoute.route('/get-comments').get([auth_jwt_token.verifyToken], (req, res) => {
    discussionModel.findOne({ _id: req.query.discussionId }, (err, discussion) => {
        if (err) {
            res.status(500).send({ message: err });
            return;
        } else {
            if (!discussion) {
                return res.status(500).send(new Error('not found!'));
            }
            res.json(discussion.comments);
        }
    }).populate({ path: 'comments', options: { sort: { 'createdAt': 1 } } });
});

discussionRoute.route('/delete-comment').delete([auth_jwt_token.verifyToken], (req, res, next) => {
    userModel.findOne({ _id: req.userId }, (err, user) => {
        if (err) {
            console.log(err);
            res.status(500).send({ message: err });
            return;
        }
        if (!user) {
            return res.status(500).send(new Error('not found!'));
        }
        roleModel.find(
            {
                _id: { $in: user.roles }
            },
            (err, roles) => {
                if (err) {
                    res.status(500).send({ message: err });
                    return;
                }
                commentModel.findOne({ _id: req.query.commentId }, (err, comment) => {
                    if (err) {
                        res.status(500).send({ message: err });
                        return;
                    } else {
                        if (!comment) {
                            return res.status(500).send(new Error('not found!'));
                        }
                        if (roles[0].name !== "admin" && comment.author !== user.username) {
                            return res.status(401).send({ message: "Unauthorized!" });
                        }
                        comment.deleted = true;
                        commentModel.findByIdAndUpdate(comment._id, {
                            $set: comment
                        }, (error) => {
                            if (error) {
                                return next(error);
                            } else {
                                res.json(comment);
                            }
                        });
                    }
                })

            }
        );
    });
});


module.exports = discussionRoute;
