 const express = require('express');
 const generalRoute = express.Router();
 const { auth_jwt_token } = require("../authentication");
 
 
 let userModel = require('../models/authentication/User');
 
 generalRoute.route('/update-settings').post([auth_jwt_token.verifyToken], (req, res) => {
    userModel.findOne({ _id: req.userId }, (err, user) => {
  
      if (err) {
        console.log(err);
        res.status(500).send({ message: err });
        return;
      }
      if (!user) {
        return res.status(500).send(new Error('not found!'));
      }
      user.settings = req.body;
      userModel.findByIdAndUpdate(req.userId, {
        $set: user
      }, (error, data) => {
        if (error) {
          return next(error);
        } else {
          res.json(data)
        }
      });
    });
  });
 
 module.exports = generalRoute;