/*
The document route API  is another important route for the basic workflows. Here are the retrievement, search and
creation of documents implemented.
*/

const express = require('express');
const documentRoute = express.Router();
const path = require('path');
const { auth_jwt_token } = require("../authentication");
let roleModel = require('../models/authentication/Role');



let documentModel = require('../models/Document');
let userModel = require('../models/authentication/User');


documentRoute.route('/').get([auth_jwt_token.verifyToken], (req, res) => {
  documentModel.find((error, data) => {
    if (error) {
      return next(error)
    } else {
      res.json(data)
    }
  })
});

documentRoute.route('/get').get([auth_jwt_token.verifyToken], (req, res, next) => {
  documentModel.findOne({ "_id": req.query.query }, (error, data) => {
    if (error) {
      return next(error)
    } else {
      if (!data) {
        return res.status(500).send(new Error('not found!'));
      }
      res.json(data);
    }
  });
});

/*
Documents can be searched by title, author or keywords and are expanded with regex. Matching documents 
will be returned.
*/
documentRoute.route('/search').post([auth_jwt_token.verifyToken], (req, res, next) => {
  documentModel.find({
    $or: [{ "title": { $regex: new RegExp(req.body.query, "i") } }, { "author": { $regex: new RegExp(req.body.query, "i") } },
    { keywords: { $elemMatch: { $regex: new RegExp(req.body.query, "i") } } }]
  }, function (err, result) {
    if (err) {
      console.log(err)
      return next(err)
    }
    if (result) {
      res.json(result)
    }

  }).limit(25)
});

/*
The upload file includes two steps. The insertion of the metadata and creation of the document object in the database.
Furthermore the acutal file will be renamed according to the new received ObjectID of the database and stored within
the file system of the server.
 */
documentRoute.route('/upload-file').post([auth_jwt_token.verifyToken], (req, res, next) => {
  userModel.findOne({ _id: req.userId }, (err, user) => {

    if (err) {
      console.log(err);
      res.status(500).send({ message: err });
      return;
    }
    if (!user) {
      return res.status(500).send(new Error('not found!'));
    }

    if (req.files) {
      var file = req.files.file;
      var filename = file.name;
      var n = filename.lastIndexOf('.');
      var fileType = filename.substring(n + 1);
      var document = JSON.parse(req.body.metadata);
      document.fileType = fileType;
      document.uploader = user.username;
      document.discussions = [];
      documentModel.create(document, (error, data) => {
        if (error) {
          console.log(error)
          return next(error)
        } else {
          var filename = './uploads/' + data._id + "." + fileType;
          file.mv(filename, function (err) {
            if (err) {
              return res.send(err)
            } else {
              return res.send(filename + " uploaded")
            }
          })
        }
      })
    } else {
      var document = JSON.parse(req.body.metadata);
      document.uploader = user.username;
      document.discussions = [];
      document.fileType = "x";
      documentModel.create(document, (error, data) => {
        if (error) {
          console.log(error)
          return next(error)
        } else {
            // return res.status(406)(send(data));
            return res.status(406).send("test-case-upload");
        }
      });
      return res.status(406);
      // return res.status(406).send(new Error('missing parameter'));
    }

  });

});

/*
All existing tags of the application can be searched with the following method looks for all inserted tags within
the documents of the database. The format of the returned object is according to the required representation of the
tag input.
*/
documentRoute.route('/search-tag').get([auth_jwt_token.verifyToken], (req, res, next) => {
  documentModel.find({ keywords: { $elemMatch: { $regex: new RegExp(req.query.query, "i") } } }, function (err, result) {
    if (err) {
      console.log(err)
      return next(err)
    }
    if (result) {
      var test = [];
      for (var i = 0; i < result.length; i++) {
        test = Array.from(new Set(test.concat(result[i].keywords)));
      }
      var formatted = [];
      for (var i = 0; i < test.length; i++) {
        formatted.push({
          "key": test[i],
          "value": test[i]
        });
      }
      res.json(formatted);
    }

  }).select('keywords').limit(25);
});


/*
Within this sub route the download of the passed documentId can be requested. The corresponding file will either get
downloaded by the user or directly displayed with the application.
 */
documentRoute.route('/download').get([auth_jwt_token.verifyToken], (req, res, next) => {
  documentModel.findOne({ "_id": req.query.query }, (err, document) => {
    if (err) {
      console.log(err);
      res.json(err);
    } else {
      if (!document) {
        return res.status(500).send(new Error('not found!'));
      }
      var fileType = document['fileType'];
      //building the exact path of the file and return it to the client.
      var filePath = (path.join(__dirname, '../uploads/' + req.query.query + "." + fileType));
      res.sendFile(filePath, function (err) {
        if (err) {
          next(err);
        }
      });
    }
  });
});

/**
 * The acutal document file can be exchanged by the uploader and administrators.
 */
documentRoute.route('/exchange-file/:id').put([auth_jwt_token.verifyToken], (req, res, next) => {
  userModel.findOne({ _id: req.userId }, (err, user) => {

    if (err) {
      console.log(err);
      res.status(500).send({ message: err });
      return;
    }
    if (!user) {
      return res.status(500).send(new Error('not found!'));
    }
    roleModel.find(
      {
        _id: { $in: user.roles }
      },
      (err, roles) => {
        if (err) {
          res.status(500).send({ message: err });
          return;
        }
        var document = JSON.parse(req.body.metadata);
        if (roles[0].name !== "admin" && document.uploader !== user.username) {
          return res.status(401).send({ message: "Unauthorized!" });
        }
        if (req.files) {
          var file = req.files.file;
          var filename = './uploads/' + document._id + "." + document.fileType;
          file.mv(filename, function (err) {
            if (err) {
              res.send(err)
            } else {
              res.send(filename + " uploaded")
            }
          })
        }
      });
  });

});

/**
 * Documents can be deleted by the uploader and administrators.
 */
documentRoute.route('/delete-document').delete([auth_jwt_token.verifyToken], (req, res, next) => {
  documentModel.findOne({ "_id": req.query.documentId }, (error, document) => {
    if (error) {
      return next(error)
    } else {
      if (!documentRoute) {
        return res.status(500).send(new Error('not found!'));
      }
      userModel.findOne({ _id: req.userId }, (err, user) => {
        if (err) {
          console.log(err);
          res.status(500).send({ message: err });
          return;
        }
        if (!user) {
          return res.status(500).send(new Error('not found!'));
        }
        roleModel.find(
          {
            _id: { $in: user.roles }
          },
          (err, roles) => {
            if (err) {
              res.status(500).send({ message: err });
              return;
            }
            //The user either has to be an admin or the author of the course
            if (roles[0].name !== "admin" && document.uploader !== user.username) {
              return res.status(401).send({ message: "Unauthorized!" });
            }
            documentModel.findByIdAndRemove(req.query.documentId, (error, data) => {
              if (error) {
                return next(error);
              } else {
                res.status(200).json({
                  msg: data
                })
              }
            })
          }
        );
      });
    }
  });
})

module.exports = documentRoute;
