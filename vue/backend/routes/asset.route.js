/**
 * This route was specified, because for a use case courses and documents were retrieved in one call. Therefore
 * the route asset was specified to split the results logically from courses and documents.
 */

const express = require('express');
const assetRoute = express.Router();
const { auth_jwt_token } = require("../authentication");


let courseModel = require('../models/Course');
let documentModel = require('../models/Document');

/*
Within the "search" sub route of assets, courses and documents can be searched and are returned in a JSON
object with courses and documents split into two attributes.
*/
assetRoute.route('/search').post([auth_jwt_token.verifyToken], (req, res, next) => {
    Promise.all([
        documentModel.find({
            $or: [{ "title": { $regex: new RegExp(req.body.query, "i") } }, { "author": { $regex: new RegExp(req.body.query, "i") } },
            { keywords: { $elemMatch: { $regex: new RegExp(req.body.query, "i") } } }]
        }),
        courseModel.find({
            $or: [{ "title": { $regex: new RegExp(req.body.query, "i") } }, { "author": { $regex: new RegExp(req.body.query, "i") } },
            { keywords: { $elemMatch: { $regex: new RegExp(req.body.query, "i") } } }]
        }),
    ]).then(([documents, courses]) => {
        var assets = {courses: courses, documents: documents,};
        res.json(assets);
    });
});

module.exports = assetRoute;