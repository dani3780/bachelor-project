/**
 * Specification of signin and singup route entry points, the regarding logic can be found in the corresponding files.
 */

const { verify_user_email } = require("../authentication")
const express = require("express")
const router = express.Router()
const auth = require("../controller/auth.controller");
let userModel = require('../models/authentication/User');
const { auth_jwt_token } = require("../authentication");


router.post("/signin", auth.signin);

router.post("/signup", 
  [
      verify_user_email.checkDuplicateUsernameOrEmail,
      verify_user_email.checkRolesExisted
  ],
  auth.signup
)


module.exports = router
