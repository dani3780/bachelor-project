let express = require('express'),
  cors = require('cors'),
  mongoose = require('mongoose'),
  database = require('./database'),
  bodyParser = require('body-parser'),
  upload = require('express-fileupload');
const createError = require('http-errors');

// Connect mongoDB
mongoose.Promise = global.Promise;
mongoose.connect(database.db, {
  useNewUrlParser: true,
  useUnifiedTopology: true,
  useFindAndModify: false
}).then(() => {
  console.log("Database connected")
},
  error => {
    console.log("Database could't be connected to: " + error)
  }
)

//specify the required APIs of the application
const courseAPI = require('../backend/routes/course.route');
const documentAPI = require('../backend/routes/document.route');
const assetAPI = require('../backend/routes/asset.route');
const discussionAPI = require('../backend/routes/discussion.route');
const generalAPI = require('../backend/routes/general.route');

const auth = require('../backend/routes/auth.route') // user authentication

const app = express();
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
  extended: false
}));
app.use(cors());
app.use(upload());

mongoose.set('useCreateIndex', true)

/*
The following APIs require the flag x-access-token to be further processed. These token furthermore will get
validated before a method will be invoked, except the sign up and sign up mechanism.
 */
app.use('/api/auth', auth, function(req, res, next){
  res.header(
    "Access-Control-Allow-Headers",
    "x-access-token, Origin, Content-Type, Accept"
  );
  next();
})
// API
app.use('/api/course', courseAPI, function(req, res, next) {
  res.header(
    "Access-Control-Allow-Headers",
    "x-access-token, Origin, Content-Type, Accept"
  );
  next();
});
app.use('/api/asset', assetAPI, function(req, res, next) {
  res.header(
    "Access-Control-Allow-Headers",
    "x-access-token, Origin, Content-Type, Accept"
  );
  next();
});
app.use('/api/document', documentAPI, function(req, res, next) {
  res.header(
    "Access-Control-Allow-Headers",
    "x-access-token, Origin, Content-Type, Accept"
  );
  next();
});
app.use('/api/discussion', discussionAPI, function(req, res, next) {
  res.header(
    "Access-Control-Allow-Headers",
    "x-access-token, Origin, Content-Type, Accept"
  );
  next();
});
app.use('/api/general', generalAPI, function(req, res, next) {
  res.header(
    "Access-Control-Allow-Headers",
    "x-access-token, Origin, Content-Type, Accept"
  );
  next();
});

// Create port
const port = process.env.PORT || 4000;
const server = app.listen(port, () => {
  console.log('Connected to port ' + port)
})


// Find 404
app.use((req, res, next) => {
  next(createError(404));
});

// error handler, by default send error with status code 500, if not defined otherwise.
app.use(function (err, req, res, next) {
  console.log(err.message);
  if (!err.statusCode) err.statusCode = 500;
  res.status(err.statusCode).send(err.message);
});

let Role = require('./models/authentication/Role');
let User = require('./models/authentication/User');
var bcrypt = require("bcryptjs");

/*
The database will initially on startup check if the roles and the admin account are inserted. Otherwise the missing
elements will get inserted. With this mechanism it can be guaranteed the roles and at least one administrator always
exist.
 */
function initialize() {
    Role.estimatedDocumentCount((err, count) => {
      if (!err && count === 0) {
        new Role({
          name: "user"
        }).create(err => {
          if (err) {
            console.log("error", err);
          }
          console.log("added 'user' to roles collection");
        });

        new Role({
          name: "admin"
        }).create(err => {
          if (err) {
            console.log("error", err);
          }  
          console.log("added 'admin' to roles collection");
        });
      }
    });
    User.findOne({"username": "admin"}, (err, res) => {
      if (res) {
        return;
      } else {
        Role.findOne({"name": "admin"}, (errRole, resRole) => {
          if (errRole) {
            console.log("Role admin could not be found, admin was not created!");
            return;
          }
          else {
            var admin = {
              username: "admin",
              email: "admin@lms.com",
              password: bcrypt.hashSync("admin", 8),
              roles: resRole._id,
            }
            User.create(admin, (errCreate, resCreate) =>
            {
              if (resCreate) {
                console.log("Admin was succesfully inserted.");
              }
              if (errCreate) {
                console.log("Error occurred creating admin!");
              }
            })
          }
        })
        
      }
    })
  }

initialize();



module.exports = app
