/*
 * DEPRECEATED
 */

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let tagSchema = new Schema({
  title: {
    type: String,
    unique: true,
    required: true,
  },

}, {
  collection: 'tag'
})

module.exports = mongoose.model('Tag', tagSchema)