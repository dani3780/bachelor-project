/*
Here the Discussion scheme is defined a the question as string, the author, an open flag, the corresponding comments
and timestamps. Each course and document includes a discussion board with an array of these discussions split into 
open and closed discussions.
*/


const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let discussionSchema = new Schema({
    question: {
        type: String
    },
    author: {
        type: String
    },
    open: {
        type: Boolean,
        default: true,
    },
    comments: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Comment'

    }],

},
    { timestamps: true },
    {
        collection: 'discussion'
    })

module.exports = mongoose.model('Discussion', discussionSchema)