/**
 * This file represents the schema of a role. Two roles exist "User" and "Admin" and
 * are used for the authorization of specific actions.
 */

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let roleSchema = new Schema({
  name: String,
},
  { timestamps: true },
  {
    collection: 'Role'
  })

module.exports = mongoose.model('Role', roleSchema)
