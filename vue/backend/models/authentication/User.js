/*
Here the User schema is defined representing a user with username, email, password and roles. The permissions
 of a user is represented by his roles.
*/

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let userSchema = new Schema({
  username: String,
  email: String,
  password: String,
  settings: {
    university: String,
  },
  roles: [
    {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Role"
    }
  ],
  courses: [
    {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Course"
    }
  ]
},
  { timestamps: true },
  {
    collection: 'User'
  })

module.exports = mongoose.model('User', userSchema)
