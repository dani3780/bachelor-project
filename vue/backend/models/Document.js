/*
Here the Document schema is defined. All required metadata fields are specified here and are the basic elements of a
course in a later step. Keywords and prerequisistes are String arrays. Discussions are discussion arrays which refer to
their schema. The isCourse is required, because in some use cases courses and documents are treated the same way in an
array as assets and the flag is set by default at creation, required to differentiate them easier.
*/


const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let documentSchema = new Schema({
  title: {
    type: String
  },
  author: {
    type: String
  },
  year: {
    type: Number
  },
  uploader: {
    type: String
  },
  university: {
    type: String
  },
  keywords: [{
    type: String
  }],
  prerequisites: [{
    type: String
  }],
  difficulty: {
    type: String
  },
  duration: {
    type: String
  },
  description: {
    type: String
  },
  language: {
    type: String
  },
  fileType: {
    type: String
  },
  isCourse: {
    type: Boolean,
    default: false,
  },
  discussions: [{
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Discussion'
  }]
}, {
  collection: 'document'
})

module.exports = mongoose.model('Document', documentSchema)