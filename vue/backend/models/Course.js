/*
Here the Course schema is defined with all required fields to describe a course in the application. Keywords and
prerequisites are assigned by a String array. Furthermore each course has an array of discussions and chapter.
Each chapter includes a course and document section. Discussions, courses and documents refer to their own schema.
*/


const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let courseSchema = new Schema({
  title: {
    type: String
  },
  author: {
    type: String
  },
  year: {
    type: String,
  },
  keywords: [{
    type: String
  }],
  prerequisites: [{
    type: String
  }],
  difficulty: {
    type: String,
  },
  duration: {
    type: String,
  },
  language: {
    type: String,
  },
  description: {
    type: String,
  },
  isCourse: {
    type: Boolean,
    default: true,
  },
  discussions: [{
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Discussion'
  }],
  chapters: [{
    name: {
      type: String,
    },
    documents: [{
      type: mongoose.Schema.Types.ObjectId,
      ref: 'Document'
    }],
    courses: [{
      type: mongoose.Schema.Types.ObjectId,
      ref: 'Course'
    }]
  }],
}, {
  collection: 'course'
})

module.exports = mongoose.model('Course', courseSchema)