/*
Here the Comment schema is defined representing a text, author, deleted flag and the created and updated timestamp.
A comment is assigned to a discussion and required for the forum in courses and documents.
*/


const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let commentSchema = new Schema({
    text: {
        type: String
    },
    author: {
        type: String
    },
    deleted: {
        type: Boolean,
        default: false,
    }
},
    { timestamps: true },
    {
        collection: 'comment'
    })

module.exports = mongoose.model('Comment', commentSchema)