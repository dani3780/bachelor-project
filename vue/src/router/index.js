import { createRouter, createWebHistory } from 'vue-router'
import Home from '../views/Home.vue'
import Course from '../views/workflows/course/Course.vue'
import Login from '../views/Login.vue'
import DocumentOverview from '../views/workflows/document/DocumentOverview.vue';
import DocumentUpload from '../views/workflows/document/DocumentUpload.vue';
import CourseCreation from '../views/workflows/course/CourseCreation.vue';
import ViewFile from '../views/workflows/document/ViewFile';
import Faq from '../views/Faq';
import Profile from '../views/Profile';




const routes = [
    {
        path: '/',
        name: 'home',
        component: Home,
    },
    {
        path: '/login',
        name: 'login',
        component: Login
    }
    ,
    {
        path: '/course/:courseId',
        name: 'course',
        component: Course,
    },
    {
        path: '/documents',
        name: 'document',
        component: DocumentOverview,
    },
    {
        path: '/documents/upload',
        name: 'documentUpload',
        component: DocumentUpload,
    },
    {
        path: '/courseCreation',
        name: 'courseCreation',
        component: CourseCreation,
    },
    {
        path: '/viewFile/:documentId',
        name: 'viewFile',
        component: ViewFile,
    },
    {
        path: '/faq',
        name: 'Faq',
        component: Faq,
    },
    {
        path: '/profile',
        name: 'Profile',
        component: Profile,
    },
    

]

const router = createRouter({
    history: createWebHistory(),
    routes: routes
})


router.beforeEach(async (to, from, next) => {
    const token = sessionStorage.getItem("user");

    document.title = to.name;
    if (to.path !== '/login' && token === null) {
        next({ path: '/login' })
    } else if (to.path === '/login' && token !== null) {
        next({ path: '/' })
    }
    else { next() }
})
export default router