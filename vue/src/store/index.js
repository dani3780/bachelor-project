import { createStore } from 'vuex';

export default createStore({
    state: {
        isUser: true,
        username: "Max Mustermann",
        courses: [
            {id: 1, title: "Introduction Databases"},
            {id: 2, title: "Introduction Databases asdf asf"},
            {id: 3, title: "Course test 2"},
            {id: 4, title: "Course test 3"},
            {id: 5, title: "Course test 4"},
            {id: 6, title: "Course test 5"},
            {id: 7, title: "Course test 6"},
            {id: 8, title: "Course test 7"},
        ]
    },

    mutations: {
        SET_USER(state, user) {
            state.isUser = user;
        }
    },

    actions: {
        setUser({ commit }, user) {
            commit('SET_USER', user)
        }
    },

    modules: {
        
    }
})