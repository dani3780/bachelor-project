import { createApp } from 'vue'
import ElementPlus from 'element-plus';
import 'element-plus/lib/theme-chalk/index.css';
import App from './App.vue'
import router from './router'
import store from './store'
import 'bootstrap/dist/css/bootstrap.min.css'
import { library } from '@fortawesome/fontawesome-svg-core'
import { faFilePdf } from '@fortawesome/free-solid-svg-icons'
import { faFile } from '@fortawesome/free-solid-svg-icons'
import { faFileAlt } from '@fortawesome/free-solid-svg-icons'
import { faFolderOpen } from '@fortawesome/free-solid-svg-icons'
import { faTrash } from '@fortawesome/free-solid-svg-icons'
import { faLock } from '@fortawesome/free-solid-svg-icons'


import './assets/css/main.css';
import axios from "axios";



import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'

//add icons from the fontawesome library.
library.add(faFilePdf)
library.add(faFile)
library.add(faFileAlt)
library.add(faFolderOpen)
library.add(faTrash)
library.add(faLock)

window.serverUrl="http://localhost:4000";

const app = createApp(App)
app.component('font-awesome-icon', FontAwesomeIcon)

app.use(router)
app.use(ElementPlus)
app.use(store)
app.mount('#app')




/*
If a unauthorized error is received, remove the user item and the user then gets redirected within the router definition. If a user uses the application,
he only sees "allowed" actions, therefore this case should actually never happen and only occurs if a user sends REST calls by the terminal to the server API.
*/
axios.interceptors.response.use(function (response) {
    return response;
}, function (error) {
    if (error.response.status === 403) {
        console.log("forbidden");
        sessionStorage.removeItem("user");
    } else {
        console.log(error.response.data.message);
        alert(error.response.data.message);
        router.push("/");
    }
});

/*
The interceptor is used here to add specific fields to each call and fill them with security values.
*/
axios.interceptors.request.use(
    request => {
        const token = sessionStorage.getItem("user");
        if (token !== null) {
            var item = JSON.parse(token);
            if (!request.url.includes('auth')) {
                request.headers['x-access-token'] = item.accessToken;
                request.headers['x-access-signature'] = item.signature;
                request.headers['access-control-allow-origin'] = null;
            }
        }
        return request;
    },
    error => {
        return Promise.reject(error);
    }
)